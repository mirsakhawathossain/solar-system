\beamer@sectionintoc {1}{Solar System}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{History of Solar System Discoveries}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Formation and Evolution}{5}{0}{1}
\beamer@sectionintoc {2}{Planetary System}{7}{0}{2}
\beamer@sectionintoc {3}{Planetary Classification}{8}{0}{3}
\beamer@sectionintoc {4}{Planets}{9}{0}{4}
\beamer@sectionintoc {5}{Theorem}{17}{0}{5}
